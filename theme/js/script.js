jQuery(document).ready(function($) {
  //close button
  $('.cancel.button').on('click', function() {
    $('.modal').modal('hide');
  });

  //mobile menu
  $('#slide-buttons').on('click', function() {
      $('.hold-inner-menu').addClass('open');
      $('body').addClass('open-modal');
    });
    $('.c-menu__close').on('click', function() {
      $('.hold-inner-menu').removeClass('open');
      $('body').removeClass('open-modal');
  });
  $(document).mouseup(function (e) {
    var container = $('.hold-inner-menu');
    if (container.has(e.target).length === 0) {
      $('.hold-inner-menu').removeClass('open');
      $('body').removeClass('open-modal');
    }
  });

  //accordion
  $(function(i) {
    var o, n;
    i(".up-accordion").on("click", function() {
      o = i(this).parents(".accordion_item"), 
      n = o.find(".info"),
      o.hasClass("active_block") ? (o.removeClass("active_block"),
      n.slideUp()) : (o.addClass("active_block"),
      n.stop(!0, !0).slideDown(),
      o.siblings(".active_block").removeClass("active_block").children(".info").stop(!0, !0).slideUp())
    });
  });

  $(function(TopMenu) {
    var i=1;
    $('#top-menu').click(function() {
      i = 1 - i;
      if ( i == 1) {
        $(".top-menu").slideUp(500);
      } else {
        $(".top-menu").slideDown(500);
      }
      return false;
    });
  });

  $('a[href^="#m"], a[href^="."]').click( function() { 
    var scroll_el = $(this).attr('href');
    if ($(scroll_el).length != 0) { 
      $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 1000);
    };
    return false; 
    $('ul li').on('mouseenter',function(e) {
      $(this).children().toggleClass('active hide');
    });     
  }); 



  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    prevText: "",           
    nextText: "",             
    animationLoop: true,
    slideshow: false,
    itemWidth: 170,
    itemMargin: 15,
    keyboard: false,
    asNavFor: '#slider'
  });
  $('#slider').flexslider({
    animation: "fade", 
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });

  $('.flexslider1').flexslider({
    animation: "slide",
    controlNav: false,
    prevText: "",           
    nextText: "",             
    animationLoop: true,
    slideshow: false,
    itemWidth: 263,
    itemMargin: 0,
    keyboard: false,
    //asNavFor: '.flexslider1-1'
  });
  $('.flexslider1-1').flexslider({
    animation: "fade", 
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    //sync: ".flexslider1"
  });

  if($('.flexslider1').length){
    var people = $('.flexslider1 .hold-people').toArray(),
        allPersonDesc = $('.flexslider1-1 > .slides > li'),
        personDesc = allPersonDesc.toArray();
    
    people.forEach(function(currentValue, index){
      currentValue.addEventListener('click', bindClick(index));
    });
    function bindClick(i) {
      return function(){
        allPersonDesc.removeClass('flex-active-slide').css({display: 'none', zIndex: 1, opacity: 0});
        $(personDesc[i]).addClass('flex-active-slide').css({display: 'block', zIndex: 2, opacity: 1});
      };
    }
  }
	 
$(".form-1").validate({
  rules: {
    name: { required: true},
    phone: { required: true},
    email: { required: true},
    message: { required: true},
  },
  messages: {
  },
  errorPlacement: function(error, element) {
  },
  submitHandler: function(form) {
    var forma =$(form);
    $.ajax({
      type: 'POST',
      url: 'sendmessage.php',
      data: forma.serialize(),
      success: function(data) {$('.form-1').find('input').val('');
      if(data == "true") {     
        $('.modal').modal('hide');   
        $('.modal-thank-you').modal('show');
        setTimeout(function(){$('.modal-thank-you').modal('hide')},5000); 
      }
    }
  });
  },
  success: function() {
  },
  highlight: function(element, errorClass) {
    $(element).addClass('error');
  },
  unhighlight: function(element, errorClass, validClass) {
    $(element).removeClass('error');
  }
}); 


    

	
});//jQuery